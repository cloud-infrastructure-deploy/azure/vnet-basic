provider "azurerm" {
  features {}
}

# State Backend
terraform {
  backend "azurerm" {
    resource_group_name  = "production_rg"
    storage_account_name = "tfstateplatformgitlab"
    container_name       = "terraform-state"
    key                  = "terraform.tfstate"
  }
}